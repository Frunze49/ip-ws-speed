#include <algorithm>
#include <vector>
#include <set>

class ipv4_subnet_static_db {
public:
    ipv4_subnet_static_db(std::vector<std::string> allowed, std::vector<std::string> disallowed) {

        struct Node {
            size_t idx;
            unsigned addr;
            bool begin;
            bool allowed;
        };
        std::vector<Node> dots;
        dots.reserve(allowed.size() * 2 + disallowed.size() * 2);
        for (size_t i = 0; i != allowed.size(); ++i) {
            ipv4_subnet addr(allowed[i]);
            uint32_t low, high;
            low = high = ntohl(addr.network.s_addr);

            uint64_t imask = (1LU << (32 - addr.length)) - 1;
            low &= ~imask;
            high |= imask;

            dots.push_back({i, low, true, true});
            dots.push_back({i, high + 1, false, true});
        }
        for (size_t i = 0; i != disallowed.size(); ++i) {
            ipv4_subnet addr(disallowed[i]);
            uint32_t low, high;
            low = high = ntohl(addr.network.s_addr);

            uint64_t imask = (1LU << (32 - addr.length)) - 1;
            low &= ~imask;
            high |= imask;
            dots.push_back({i, low, true, false});
            dots.push_back({i, high + 1, false, false});
        }
        std::sort(dots.begin(), dots.end(), [](const Node &l, const Node &r) {
            return std::tie(l.addr, l.allowed, l.idx) < std::tie(r.addr, r.allowed, r.idx);
        });

        std::set<size_t> allowed_opened, disallowed_opened;
        bool was_allowed = false;
        for (auto it = dots.begin(); it != dots.end(); ++it) {
            const auto & elem = *it;
            if (elem.allowed) {
                if (elem.begin) {
                    allowed_opened.insert(elem.idx);
                } else {
                    allowed_opened.erase(elem.idx);
                }
            } else {
                if (elem.begin) {
                    disallowed_opened.insert(elem.idx);
                } else {
                    disallowed_opened.erase(elem.idx);
                }
            }

            if (it + 1 == dots.end() || (it + 1)->addr != it->addr) {
                bool new_allowed = disallowed_opened.empty() && !allowed_opened.empty();
                if (new_allowed != was_allowed) {
                    was_allowed = new_allowed;
                    indexes.push_back(elem.addr);
                }
            }
        }
    }

  auto is_allowed(uint32_t const addr) -> bool
  {
        return (std::upper_bound(indexes.begin(), indexes.end(), addr) - indexes.begin()) % 2 != 0;
  }

  auto is_allowed(std::string const &addr) -> bool
  {
    in_addr ip_addr{};
    auto r = ::inet_aton(addr.c_str(), &ip_addr);
    return r == 1 && is_allowed(ntohl(ip_addr.s_addr));
  }

private:
    std::vector<unsigned> indexes;
};


struct Builder {
    void add(std::string s, bool allow) {
        if (allow) {
            allowed.push_back(s);
        } else {
            disallowed.push_back(s);
        }
    }

    ipv4_subnet_static_db build() const {
        return ipv4_subnet_static_db(allowed, disallowed);
    }

    std::vector<std::string> allowed;
    std::vector<std::string> disallowed;
};
