#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <chrono>
#include <cassert>
#include <functional>
#include <iostream>
#include <sstream>
#include <vector>
#include <list>
#include <string>
#include <limits>
#include <random>
#include <ranges>
#include <stdexcept>
#include <fstream>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <iomanip>
#include <bitset>
#include <atomic>
#include <thread>

#include <boost/program_options.hpp>

#include "fastnetmon/patricia.hpp"
#include "ipv4_interval_db.hpp"
#include "ipv4_static_db.hpp"
#include "used_memory.cpp"

using namespace std;
namespace po = boost::program_options;

std::vector<uint32_t> cidr_to_ips(const std::string& cidr) {
    std::vector<uint32_t> ips;

    size_t slash_pos = cidr.find('/');
    if (slash_pos == std::string::npos) {
        std::cerr << "Invalid CIDR format: " << cidr << std::endl;
        return ips;
    }

    std::string network_str = cidr.substr(0, slash_pos);
    std::string prefix_len_str = cidr.substr(slash_pos + 1);
    int prefix_len = std::stoi(prefix_len_str);

    in_addr network_addr{};
    inet_pton(AF_INET, network_str.c_str(), &network_addr.s_addr);
    uint32_t network = ntohl(network_addr.s_addr);

    uint32_t mask = (0xFFFFFFFFu << (32 - prefix_len));
    uint32_t start_ip = network & mask;
    uint32_t end_ip = start_ip | ~mask;

    for (uint32_t ip = start_ip; ip <= end_ip; ++ip) {
        ips.push_back(ip);
    }

    return ips;
}


auto gen_test_vector(uint32_t percent, std::vector<string>& all_subnets, unsigned nitems) -> std::list<uint32_t>
{
    assert(percent <= 100);
    assert(percent >= 0);

    std::list<uint32_t> ret;

/*     // NOLINTNEXTLINE - random engine with fixed seed */
    std::default_random_engine engine(time(NULL));

    std::vector<uint32_t> all_ips;

    for (const auto& subnet : all_subnets) {
        auto ips = cidr_to_ips(subnet);
        all_ips.insert(all_ips.end(), ips.begin(), ips.end());
    }
    std::set<uint32_t> all_ips_set(all_ips.begin(), all_ips.end());

    assert(all_ips.size() > 0);

    std::uniform_int_distribution<uint32_t> rand_ip(0, std::numeric_limits<uint32_t>::max());
    std::uniform_int_distribution<uint32_t> rand_int(1, 100);
    std::uniform_int_distribution<uint32_t> rand_index(0, all_ips.size() - 1);

    for (unsigned i = 0; i < nitems; ++i) {
        if (percent < rand_int(engine)) {
            uint32_t item = rand_ip(engine);
            while(all_ips_set.contains(item)) {
                item = rand_ip(engine);
            }
            ret.emplace_back(item);
        } else {
            uint32_t item_index = rand_index(engine);
            ret.emplace_back(all_ips[item_index]);
        }

    }

    return ret;
}

void process_patricia(std::list<uint32_t>::const_iterator begin, std::list<uint32_t>::const_iterator end, patricia_tree_t* lookup_tree, std::atomic<uint32_t>& checkAllowed, std::atomic<uint32_t>& checkForbidden) {
    prefix_t prefix_for_check_adreess;
    prefix_for_check_adreess.family     = AF_INET;
    prefix_for_check_adreess.bitlen     = 32;

    for (auto it = begin; it != end; ++it) {
        uint32_t candidate = *it;
        prefix_for_check_adreess.add.sin.s_addr = htonl(candidate);
        patricia_node_t* found_patrica_node = patricia_search_best2(lookup_tree, &prefix_for_check_adreess, 1);  

        if (found_patrica_node) {
            checkAllowed.fetch_add(1);
        } else {
            checkForbidden.fetch_add(1);
        }
    }
}

void process_interval(std::list<uint32_t>::const_iterator begin, std::list<uint32_t>::const_iterator end, ipv4_interval_db* subnet_db2, std::atomic<uint32_t>& checkAllowed, std::atomic<uint32_t>& checkForbidden) {
    for (auto it = begin; it != end; ++it) {
        uint32_t candidate = *it;
        if (subnet_db2->is_allowed(candidate)) {
            checkAllowed.fetch_add(1);
        } else {
            checkForbidden.fetch_add(1);
        }
    }
}

void process_interval2(std::list<uint32_t>::const_iterator begin, std::list<uint32_t>::const_iterator end, ipv4_interval_db* subnet_db2, std::atomic<uint32_t>& checkAllowed, std::atomic<uint32_t>& checkForbidden) {
    for (auto it = begin; it != end; ++it) {
        uint32_t candidate = *it;
        if (subnet_db2->is_allowed2(candidate)) {
            checkAllowed.fetch_add(1);
        } else {
            checkForbidden.fetch_add(1);
        }
    }
}

void process_interval3(std::list<uint32_t>::const_iterator begin, std::list<uint32_t>::const_iterator end, ipv4_interval_db* subnet_db2, std::atomic<uint32_t>& checkAllowed, std::atomic<uint32_t>& checkForbidden) {
    for (auto it = begin; it != end; ++it) {
        uint32_t candidate = *it;
        if (subnet_db2->is_allowed3(candidate)) {
            checkAllowed.fetch_add(1);
        } else {
            checkForbidden.fetch_add(1);
        }
    }
}

void process_static(std::list<uint32_t>::const_iterator begin, std::list<uint32_t>::const_iterator end, ipv4_subnet_static_db* subnet_db3, std::atomic<uint32_t>& checkAllowed, std::atomic<uint32_t>& checkForbidden) {
    for (auto it = begin; it != end; ++it) {
        uint32_t candidate = *it;
        if (subnet_db3->is_allowed(candidate)) {
            checkAllowed.fetch_add(1);
        } else {
            checkForbidden.fetch_add(1);
        }
    }
}

void finish_test(std::chrono::steady_clock::time_point start, uint32_t checkAllowed, uint32_t checkForbidden) {
    std::cout << "Elapsed(ms)=" << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() << std::endl;
    std::cout << "Allowed: " << checkAllowed << "; Forbidden: " << checkForbidden<< std::endl;
}

void test_perf(std::list<uint32_t>& data, patricia_tree_t* lookup_tree, ipv4_interval_db& subnet_db2, ipv4_subnet_static_db& subnet_db3, int numThreads = 1) {
    std::atomic<uint32_t> checkAllowed = 0, checkForbidden = 0;
    auto start = std::chrono::steady_clock::now();
    std::vector<std::thread> threads;
    size_t chunkSize = data.size() / numThreads;
    auto it = data.begin();

    {
        cout << "Performing access check emulation..." << endl;

        for (int i = 0; i < numThreads; ++i) {
            auto next = std::next(it, chunkSize);
            threads.emplace_back(process_patricia, it, next, lookup_tree, std::ref(checkAllowed), std::ref(checkForbidden));
            it = next;
        }

        for (auto& t : threads) {
            t.join();
        }
        threads.clear();
        finish_test(start, checkAllowed, checkForbidden);   
    }
    {
        cout << "Performing access check emulation (intervals_set)..." << endl;
        checkAllowed = 0;
        checkForbidden = 0;

        start = std::chrono::steady_clock::now();
        it = data.begin();

        for (int i = 0; i < numThreads; ++i) {
            auto next = std::next(it, chunkSize);
            threads.emplace_back(process_interval, it, next, &subnet_db2, std::ref(checkAllowed), std::ref(checkForbidden));
            it = next;
        }

        for (auto& t : threads) {
            t.join();
        }
        threads.clear();
        finish_test(start, checkAllowed, checkForbidden);
    }
    {
        cout << "Performing access check emulation (intervals & lower_bound)..." << endl;
        checkAllowed = 0;
        checkForbidden = 0;

        start = std::chrono::steady_clock::now();
        it = data.begin();

        for (int i = 0; i < numThreads; ++i) {
            auto next = std::next(it, chunkSize);
            threads.emplace_back(process_interval2, it, next, &subnet_db2, std::ref(checkAllowed), std::ref(checkForbidden));
            it = next;
        }

        for (auto& t : threads) {
            t.join();
        }
        threads.clear();
        finish_test(start, checkAllowed, checkForbidden);
    }
    {
        cout << "Performing access check emulation (intervals & binary)..." << endl;
        checkAllowed = 0;
        checkForbidden = 0;

        start = std::chrono::steady_clock::now();

        it = data.begin();
        for (int i = 0; i < numThreads; ++i) {
            auto next = std::next(it, chunkSize);
            threads.emplace_back(process_interval3, it, next, &subnet_db2, std::ref(checkAllowed), std::ref(checkForbidden));
            it = next;
        }

        for (auto& t : threads) {
            t.join();
        }
        threads.clear();
        finish_test(start, checkAllowed, checkForbidden);
    }
    {
        cout << "Performing access check emulation (static intervals)..." << endl;
        checkAllowed = 0;
        checkForbidden = 0;

        start = std::chrono::steady_clock::now();

        it = data.begin();
        for (int i = 0; i < numThreads; ++i) {
            auto next = std::next(it, chunkSize);
            threads.emplace_back(process_static, it, next, &subnet_db3, std::ref(checkAllowed), std::ref(checkForbidden));
            it = next;
        }

        for (auto& t : threads) {
            t.join();
        }
        threads.clear();
        finish_test(start, checkAllowed, checkForbidden);
    }
}

int main(int argc, char* argv[])
{
    int num_threads;
    int percent;
    unsigned data_size;

    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Get help")
        ("threads,t", po::value<int>(&num_threads)->default_value(1), "Threads number")
        ("percent,p", po::value<int>(&percent)->default_value(50), "Percent of allowed data in traffic")
        ("size,s",    po::value<unsigned>(&data_size)->default_value(50000000), "Size of traffic");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    std::cout << "Threads number: " << num_threads << std::endl;
    std::cout << "Percent of allowed data in traffic: " << percent << std::endl;
    std::cout << "Size of traffic: " << data_size << std::endl;

    ipv4_interval_db subnet_db2;

    auto db3_builder = Builder();

    patricia_tree_t* lookup_tree;
    lookup_tree = New_Patricia(32);

    std::vector<string> all_subnets;
    all_subnets.push_back("10.0.0.0/8");
    all_subnets.push_back("172.16.0.0/16");
    all_subnets.push_back("192.168.10.0/24");

    for(int i = 1; i <= 254; ++i)
    {
        ostringstream s;
        s << "192.168.20." << int(i) << "/32";
        all_subnets.push_back(s.str());
    }
    for(int i = 1; i <= 254; ++i)
    {
        ostringstream s;
        s << "192.168.30." << int(i) << "/32";
        all_subnets.push_back(s.str());
    }

    for (auto s : all_subnets) {
        const char* cstr = s.c_str();
        make_and_lookup(lookup_tree, cstr);
        subnet_db2.add(s, true);
        db3_builder.add(s, true);
    }


    auto subnet_db3 = db3_builder.build();

    cout << "Preparing sample data..." << endl;

    auto data = gen_test_vector(percent, all_subnets, data_size);

    test_perf(data, lookup_tree, subnet_db2, subnet_db3, num_threads);

    Destroy_Patricia(lookup_tree, [](void* ptr) {});

    cout << "End of story!" << endl;

}
