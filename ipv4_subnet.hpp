#include <arpa/inet.h>
#include <string>
#include <stdexcept>
#include <fstream>

struct ipv4_subnet {
    in_addr network{};
    int length = 32;

    ipv4_subnet(char const *s) : ipv4_subnet(std::string(s)) {} // NOLINT

    ipv4_subnet(std::string s) // NOLINT
    {
        auto slash = s.find('/');

        if (slash != std::string::npos) {
            length = std::stoi(s.substr(slash + 1));
            s = s.substr(0, slash);
        }

        auto r = ::inet_pton(AF_INET, s.c_str(), &network);
        if (r != 1)
            throw std::invalid_argument("invalid address: " + s);
    }
/* 
    bool operator<(const ipv4_subnet& other) const
    {
        return this->length < other.length && this->network.s_addr < other.network.s_addr;
    }
 */
};

auto operator==(ipv4_subnet const &a, ipv4_subnet const &b)
{
    return a.network.s_addr == b.network.s_addr &&
           a.length == b.length;
}

auto operator<<(std::ostream &strm, ipv4_subnet const &addr) -> std::ostream &
{
    char buf[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &addr.network.s_addr, buf, sizeof(buf));
    strm << buf;
    strm << '/' << addr.length;
    return strm;
}