#include <boost/icl/interval_set.hpp>
#include <cstdint>
#include <algorithm>

#include "ipv4_subnet.hpp"

using namespace boost::icl;

class  ipv4_interval_db {
public:
	auto add(const char *addr, bool allowed) -> bool
	{
		return add(ipv4_subnet(addr), allowed);
	}

	auto add(std::string const &addr, bool allowed) -> bool
	{
		return add(ipv4_subnet(addr), allowed);
	}

	auto add(ipv4_subnet const &addr, bool allowed) -> bool
	{
		uint32_t low, high;
		low = high = ntohl(addr.network.s_addr);

		uint64_t imask = (1LU << (32 - addr.length)) - 1;
		low &= ~imask;
		high |= imask;

		if (allowed) {
			ip_allowed.insert(discrete_interval<uint32_t>(low, high, interval_bounds::closed()));
		} else {
			ip_forbidden.insert(discrete_interval<uint32_t>(low, high, interval_bounds::closed()));
		}

		/* So dirty... */
		ip_total = ip_allowed - ip_forbidden;

		left.clear();
		right.clear();
		left.reserve(ip_total.size());
		right.reserve(ip_total.size());

		for (const auto interval: ip_total) {
			left.push_back(interval.lower());
			right.push_back(interval.upper());
		}

		return allowed;
	}

	auto is_allowed(uint32_t const addr) -> bool
	{
		return ip_total.find(addr) != ip_total.end();
	}

	auto is_allowed2(uint32_t const addr) -> bool
	{
		const uint32_t value = addr;

		if (left.empty())
			return false;

		if (value < *left.begin() || value > *(right.end() - 1))
			return false;

		auto found_candidate_r = std::lower_bound(right.begin(), right.end(), value);

		if (found_candidate_r == right.end())
			return false;

		return value >= *(left.begin() + std::distance(right.begin(), found_candidate_r));
	}

	auto is_allowed3(uint32_t const addr) -> bool
	{
		const uint32_t value = addr;
		const auto right_ptr = right.data();
		const auto size = right.size();

		if (!size)
			return false;

		if (value < *left.begin() || value > right_ptr[size - 1])
			return false;

		size_t l = 0, r = size - 1;

		while (r - l > 1) {
			if (right_ptr[(l + r) / 2] > value)
				r = (l + r) / 2;
			else
				l = (l + r) / 2;
		}

		if (right_ptr[l] < value)
			l = r;

		return left.at(l) <= value;
	}

	auto is_allowed(std::string const &addr) -> bool
	{
		in_addr ip_addr{};
		auto r = ::inet_aton(addr.c_str(), &ip_addr);
		return r == 1 && ip_total.find(ntohl(ip_addr.s_addr)) != ip_total.end();
	}

private:
	interval_set<uint32_t> ip_allowed;
	interval_set<uint32_t> ip_forbidden;

	interval_set<uint32_t> ip_total;

	std::vector<uint32_t> left, right;
};
