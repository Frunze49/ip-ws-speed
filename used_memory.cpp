#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <string.h>
#include <vector>
#include <iostream>

long GetPrivateWorkingSet(const char* process) {
    FILE* file;
    char filename[32];
    char line[128];
    snprintf(filename, sizeof(filename), "/proc/%s/status", process);
    file = fopen(filename, "r");
    long private_working_set = -1;
    if (file) {
        while (fgets(line, sizeof(line), file)) {
            if (strncmp(line, "VmRSS:", 6) == 0) {
                sscanf(line, "%*s %ld", &private_working_set);
                break;
            }
        }
        fclose(file);
    }
    return private_working_set * 1024; // Convert from kB to bytes
}

long UsedMemory() {
    struct sysinfo memInfo;
    sysinfo(&memInfo);
    long totalRAM = memInfo.totalram;
    long freeRAM = memInfo.freeram;
    long usedRAM = totalRAM - freeRAM;

    long privateWorkingSet = GetPrivateWorkingSet("self");
    if (privateWorkingSet != -1)
        return privateWorkingSet;
    else
        printf("Error getting private working set\n");
        return -1;
}